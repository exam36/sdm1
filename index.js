const express = require('express');
const app = express()
const cors =require('cors')
app.use(express.json())
app.use(cors('*'))

const mysql = require('mysql2')
const connection= mysql.createConnection({
    'host':'host.docker.internal',
    'port':3306,
    'user':'root',
    'password':'Gib@1989',
    'database': 'movie'
});

app.get('/movie',(req,resp)=>{
    const query = 'select * from movies';
    connection.execute(query,[],(err,res)=>{
       if(err){
        resp.send(err);
       }else{
        resp.send(res);
       }
    })
})
app.post('/movie',(req,resp)=>{
    const {movie_id, movie_title, movie_release_date,movie_time,director_name} = req.body;
    const query = 'insert into movies(movie_id, movie_title, movie_release_date,movie_time,director_name) values(?,?,?,?,?)';
    connection.execute(query,[movie_id, movie_title, movie_release_date,movie_time,director_name],(err,res)=>{
       if(err){
        resp.send(err);
       }else{
        resp.send(res);
       }
    })
})
app.delete('/movie/:movie_id',(req,resp)=>{
    const {movie_id} = req.params;
    const query = 'delete from movies where movie_id=?';
    connection.execute(query,[movie_id],(err,res)=>{
       if(err){
        resp.send(err);
       }else{
        resp.send(res);
       }
    })
})

app.listen(8000,'0.0.0.0',()=>{
    console.log('Server started on 8000')
})